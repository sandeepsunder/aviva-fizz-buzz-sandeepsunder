﻿using AvivaFizzBuzzCode.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvivaFizzBuzzCode.Tests
{
    [TestClass]
    public class FizzBuzzTester
    {
        [TestMethod]
        public void DisplayResultTest()
        {
            FizzBuzzController controllerObj = new FizzBuzzController();
            var tstResult = controllerObj.DisplayResult();
            Assert.AreEqual("DisplayResult", tstResult.ViewName);
        }
        [TestMethod]
        public void GetWordfortheDayTest()
        {
            WordSelector ws = new WordSelector();
            string result = ws.GetWordfortheDay("Fizz");
            int day = (int)System.DateTime.Now.DayOfWeek;

            if (day == 3)
                Assert.AreEqual("Wizz", result);
            else
                Assert.AreEqual("Fizz", result);
        }
    }
}
