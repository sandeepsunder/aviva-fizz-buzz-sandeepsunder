﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvivaFizzBuzzCode
{
    public class WordSelector
    {
        int day = (int)System.DateTime.Now.DayOfWeek;
        public string GetWordfortheDay(string FizzOrBuzz)
        {
            string rtrnString = string.Empty;

            switch (day)
            {
                case 1:
                case 2:
                case 4:
                case 5:
                case 6:
                case 7:
                    rtrnString = FizzOrBuzz;
                    break;
                case 3:
                    if (FizzOrBuzz == "Fizz")
                        rtrnString = "Wizz";
                    else
                        rtrnString = "Wuzz";

                    break;

                default:
                    rtrnString = FizzOrBuzz;
                    break;
            }
            return rtrnString;
        }
    }
}