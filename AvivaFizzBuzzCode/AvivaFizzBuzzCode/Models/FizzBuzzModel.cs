﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvivaFizzBuzzCode.Models
{
    public class FizzBuzzModel
    {
        public int MaxLimit { get; set; }

        public List<KeyValuePair<string, string>> FizzBussList { get; set; }

        public string Color { get; set; }
    }
}