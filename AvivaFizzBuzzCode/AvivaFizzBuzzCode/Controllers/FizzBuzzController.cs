﻿using AvivaFizzBuzzCode.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvivaFizzBuzzCode.Controllers
{
    public class FizzBuzzController : Controller
    {
        //
        // GET: /FizzBuzz/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MyFizzBuzz()
        {
            return View();
        }
        public ViewResult DisplayResult()
        {
            FizzBuzzModel fizzbuzzmodel = new FizzBuzzModel();

            fizzbuzzmodel.MaxLimit = Convert.ToInt32(Request.Form["MaxLimit"]);
            fizzbuzzmodel.FizzBussList = new List<KeyValuePair<string, string>>();

            WordSelector ws = new WordSelector();

            for (int count = 1; count <= fizzbuzzmodel.MaxLimit; count++)
            {
                string result = string.Empty;
                string Color = string.Empty;
                KeyValuePair<string, string> keyval;
                if (count % 3 == 0)
                {
                    result += ws.GetWordfortheDay("Fizz");
                    Color = "color:#0000CC ;";
                }
                if (count % 5 == 0)
                {
                    result += ws.GetWordfortheDay("Buzz"); ;
                    Color = "color:#009900 ;";
                }
                if (string.IsNullOrEmpty(result))
                {
                    result = count.ToString();
                    Color = "color:#000000 ;";
                }

                keyval = new KeyValuePair<string, string>(result, Color);
                fizzbuzzmodel.FizzBussList.Add(keyval);
            }

            return View(fizzbuzzmodel);
        }

        private void CallWebservicetoLog(string uservalue)
        {
            var mockDataAccess = new Mock<IWebServiceInterfacecs>();
            //mockDataAccess.Setup(m => m.CallWebserviceTolog(uservalue));
        }

    }
}
